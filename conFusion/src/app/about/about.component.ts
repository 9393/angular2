import { Component, OnInit } from '@angular/core';

import { LeaderService } from '../services/leader.service';
import { Leader} from '../shared/leader';
import { flyInOut } from '../animations/app.animation';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut()
  ]
})
export class AboutComponent implements OnInit {
leader: Leader;
  constructor(private leaderservice: LeaderService) { }

  ngOnInit() {
    let leader = this.leaderservice.getLeaders() 
  }

}
